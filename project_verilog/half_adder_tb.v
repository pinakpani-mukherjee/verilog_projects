`include "half_adder.v"
module half_adder_tb;
 
  parameter STEP=10;
  reg r_BIT1 = 0;
  reg r_BIT2 = 0;
  wire w_SUM;
  wire w_CARRY;
   
  half_adder half_adder_inst
    (
     .i_bit1(r_BIT1),
     .i_bit2(r_BIT2),
     .o_sum(w_SUM),
     .o_carry(w_CARRY)
     );
 
  initial
    begin
    $dumpfile("half_adder_tb.vcd");
    $dumpvars(0,half_adder_tb);
    $monitor("%t: A=%b, B=%b, sum=%b, carry=%b",$time,r_BIT1,r_BIT2,w_SUM,w_CARRY);
      r_BIT1 = 1'b0;
      r_BIT2 = 1'b0;
      #STEP
      r_BIT1 = 1'b0;
      r_BIT2 = 1'b1;
      #STEP
      r_BIT1 = 1'b1;
      r_BIT2 = 1'b0;
      #STEP
      r_BIT1 = 1'b1;
      r_BIT2 = 1'b1;
      #STEP
        $finish;
    end 
 
endmodule // half_adder_tb