`include "ex_1.v"

module ex_1_tb;
parameter STEP=10;
reg [2:0]in;
wire out;
integer i;

ex_1 ex(in[2],in[1],in[0],out);

initial begin
    $dumpfile("ex_1.vcd");
    $dumpvars(0,ex_1_tb);
    $monitor("%t: A=%b, B=%b, C=%b, fa=%b",$time,in[2],in[1],in[0],out);

in<=3'b000;
for(i=0;i<7;i=i+1)begin
#STEP   
    in<=in+1'b1;
end
#STEP   
    $finish;

end

endmodule