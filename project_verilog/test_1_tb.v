`include "test_1.v"


module test_1_tb;
parameter STEP=10;
reg inA,inB;
wire out;

test_1 ex(inA,inB,out);

initial begin
    $dumpfile("test_1.vcd");
    $dumpvars(0,test_1_tb);
    $monitor("%t: A=%b, B=%b, fa=%b",$time,inA,inB,out);

    inA<=1'b0; inB<=1'b0;
#STEP
    inA<=1'b0; inB<=1'b1;
#STEP
    inA<=1'b1; inB<=1'b0;
#STEP
    inA<=1'b1; inB<=1'b1;
#STEP
    $finish;
end

endmodule