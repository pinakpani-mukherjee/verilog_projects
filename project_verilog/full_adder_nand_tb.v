`include "full_adder_nand.v"
module full_adder_nand_tb;
 
  parameter STEP=10;
  reg r_BIT1 = 0;
  reg r_BIT2 = 0;
  reg r_BITC = 0;
  wire w_SUM;
  wire w_CARRY;
   
  full_adder_nand full_adder_nand_inst
    (
     .i_bit1(r_BIT1),
     .i_bit2(r_BIT2),
     .i_carry(r_BITC),
     .o_sum(w_SUM),
     .o_carry(w_CARRY)
     );
 
  initial
    begin
    $dumpfile("full_adder_nand_tb.vcd");
    $dumpvars(0,full_adder_nand_tb);
    $monitor("%t: A=%b, B=%b, in_carry=%b,, sum=%b, carry=%b",$time,r_BIT1,r_BIT2,r_BITC,w_SUM,w_CARRY);
      r_BIT1 = 1'b0;
      r_BIT2 = 1'b0;
      r_BITC = 1'b0;
      #STEP
      r_BIT1 = 1'b0;
      r_BIT2 = 1'b1;
      r_BITC = 1'b0;
      #STEP
      r_BIT1 = 1'b1;
      r_BIT2 = 1'b0;
      r_BITC = 1'b0;
      #STEP
      r_BIT1 = 1'b1;
      r_BIT2 = 1'b1;
      r_BITC = 1'b0;
      #STEP
      r_BIT1 = 1'b0;
      r_BIT2 = 1'b0;
      r_BITC = 1'b1;
      #STEP
      r_BIT1 = 1'b0;
      r_BIT2 = 1'b1;
      r_BITC = 1'b1;
      #STEP
      r_BIT1 = 1'b1;
      r_BIT2 = 1'b0;
      r_BITC = 1'b1;
      #STEP
      r_BIT1 = 1'b1;
      r_BIT2 = 1'b1;
      r_BITC = 1'b1;
      #STEP
        $finish;
    end 
 
endmodule // full_adder_nand_tb