`include "test_2.v"


module test_2_tb;
parameter STEP=10;
reg [3:0]in;
wire out;
integer i;

test_2 ex(in[3],in[2],in[1],in[0],out);

initial begin
    $dumpfile("test_2.vcd");
    $dumpvars(0,test_2_tb);
    $monitor("%t: A=%b, B=%b, C=%b, D=%b, fa=%b",$time,in[3],in[2],in[1],in[0],out);

in<=4'b0000;
for(i=0;i<15;i=i+1)begin
#STEP   
    in<=in+1'b1;
end
#STEP   
    $finish;

end

endmodule