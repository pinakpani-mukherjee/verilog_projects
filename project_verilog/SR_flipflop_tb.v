`include "SR_flipflop.v"
module SR_flipflop_tb;
 
  parameter STEP=10;
  reg r_S = 0;
  reg r_R = 0;
  wire w_Q;
  wire w_Q_bar;
   
  SR_flipflop SR_flipflop_inst
    (
     .S(r_S),
     .R(r_R),
     .Q(w_Q),
     .Q_bar(w_Q_bar)
     );
 
  initial
    begin
    $dumpfile("SR_flipflop_tb.vcd");
    $dumpvars(0,SR_flipflop_tb);
    $monitor("%t: S=%b, R=%b, Q=%b, Q_bar=%b",$time,r_S,r_R,w_Q,w_Q_bar);
      r_S = 1'b0;
      r_R = 1'b0;
      #STEP
      r_S = 1'b0;
      r_R = 1'b1;
      #STEP
      r_S = 1'b1;
      r_R = 1'b0;
      #STEP
      r_S = 1'b1;
      r_R = 1'b1;
      #STEP
        $finish;
    end 
 
endmodule // SR_flipflop_tb