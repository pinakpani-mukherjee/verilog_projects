`include "JK_flipflop.v"
module JK_flipflop_tb;
reg J = 0,K = 0, CLK;
wire Q, QBAR;


JK_flipflop JK_flipflop_inst(.q(Q), .qbar(QBAR), .j(J), .k(K), .clk(CLK)); 



initial begin 
$dumpfile("JK_flipflop_tb.vcd");
$dumpvars(0,JK_flipflop_tb);
$monitor("simtime = %g, CLK = %b, J = %b, K = %b, Q = %b, QBAR = %b", $time, CLK, J, K, Q, QBAR);
  CLK=0;
     forever #10 CLK = ~CLK;  
end 
initial begin 
J = 1'b1;
K = 1'b0;
#100
J = 1'b0;
K = 1'b1;
#100
J = 1'b0;
K = 1'b0;
#100
J = 1'b1;
K = 1'b1;
#100
$finish;

end 
endmodule