module ex_1(A,B,C,fa);
    input A,B,C;
    output fa;
    assign fa = ((A|B)&(A|C));
endmodule