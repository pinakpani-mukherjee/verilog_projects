module JK_flipflop_6nand(q,qbar,clk,j,k);

input j,k,clk,rbar;
output q,qbar;



wire w1,w2,w3,w4;

nand(w1,j,clk,w2,w3);
nand(w3,w1,q);
nand(w2,k,clk,w4);
nand(w4,w3,w2);
nand(q,w3,qbar);
nand(qbar,q,rbar,w2);




endmodule
