module full_adder_nand 
  (
   i_bit1,
   i_bit2,
   i_carry,
   o_sum,
   o_carry
   );
 
  input  i_bit1;
  input  i_bit2;
  input  i_carry;
  output o_sum;
  output o_carry;
 
 // wire   w_WIRE_1;
 // wire   w_WIRE_2;
 // wire   w_WIRE_3;
       
 // assign w_WIRE_1 = i_bit1 ^ i_bit2;
 // assign w_WIRE_2 = w_WIRE_1 & i_carry;
 // assign w_WIRE_3 = i_bit1 & i_bit2;
 
 // assign o_sum   = w_WIRE_1 ^ i_carry;
 // assign o_carry = w_WIRE_2 | w_WIRE_3;
 
 
  
    assign o_sum   = i_bit1 ^ i_bit2 ^ i_carry;
    assign o_carry = ((i_bit1 ^ i_bit2) & i_carry) | (i_bit1 & i_bit2);
 
wire w1,w2,w3,w4,w6,w7,w8;

nand(w8,i_bit1,i_bit2);
nand(w7,w8,i_bit2);
nand(w6,i_bit1,w8);
or(w4,w6,w7);
nand(w3,i_carry,w4);
nand(w2,w3,i_carry);
nand(w1,w3,w4);
or(o_sum,w1,w2);
or(o_carry,w3,w8);
   
endmodule // full_adder_nand