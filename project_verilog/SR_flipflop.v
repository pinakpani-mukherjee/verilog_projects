module SR_flipflop 
  (
   S,
   R,
   Q,
   Q_bar,
   );
 
  input  S;
  input  R;
  output Q;
  output Q_bar;
 

nand(Q,~(S),Q_bar);
nand(Q_bar,~(R),Q);
   
endmodule // SR_flipflop