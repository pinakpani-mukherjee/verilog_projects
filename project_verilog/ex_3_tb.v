`include "ex_3.v"

module ex_3_tb;
parameter STEP=10;
reg [1:0]in;
wire out;
integer i;

ex_3 ex(in[1],in[0],out);

initial begin
    $dumpfile("ex_3.vcd");
    $dumpvars(0,ex_3_tb);
    $monitor("%t: A=%b, B=%b, fa=%b",$time,in[1],in[0],out);

in<=2'b00;
for(i=0;i<3;i=i+1)begin
#STEP   
    in<=in+1'b1;
end
#STEP   
    $finish;

end

endmodule