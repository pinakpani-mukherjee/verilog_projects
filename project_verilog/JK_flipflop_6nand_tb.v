`include "JK_flipflop_6nand.v"

module JK_flipflop_6nand_tb;
reg J,K,RBAR, CLK;
wire Q, QBAR;



JK_flipflop_6nand JK_flipflop_6nand_inst(.q(Q), .qbar(QBAR), .j(J), .k(K), .clk(CLK)); 

initial begin
$dumpfile("JK_flipflop_6nand_tb.vcd");
$dumpvars(0,JK_flipflop_6nand_tb);
$monitor("simtime = %g, CLK = %b, J = %b, K = %b, Q = %b, QBAR = %b, Reset = %b", $time, CLK, J, K, Q, QBAR,RBAR);
end
always begin
  CLK=0;
     forever #10 CLK = ~CLK;  
end 
initial begin
RBAR = 1'b1; 
J = 1'b1;
K = 1'b0;
#100
J = 1'b0;
K = 1'b1;
#100
J = 1'b0;
K = 1'b0;
#100
J = 1'b1;
K = 1'b1;
#100
RBAR = 1'b0;
J = 1'b1;
K = 1'b0;
#100
J = 1'b0;
K = 1'b1;
#100
J = 1'b0;
K = 1'b0;
#100
J = 1'b1;
K = 1'b1;
#100
$finish;

end 
endmodule