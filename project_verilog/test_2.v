module test_2(A,B,C,D,fa);
    input A,B,C,D;
    output fa;
    assign fa = ~(~(A&B) & ~(C&D));
endmodule