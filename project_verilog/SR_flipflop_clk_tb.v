`include "SR_flipflop_clk.v"

module SR_flipflop_clk_tb;
reg S,R, CLK;
wire Q, QBAR;



SR_flipflop_clk SR_flipflop_clk_inst(.q(Q), .qbar(QBAR), .s(S), .r(R), .clk(CLK)); 

initial begin
$dumpfile("SR_flipflop_clk_tb.vcd");
$dumpvars(0,SR_flipflop_clk_tb);
$monitor("simtime = %g, CLK = %b, S = %b, R = %b, Q = %b, QBAR = %b", $time, CLK, S, R, Q, QBAR);
  CLK=0;
     forever #10 CLK = ~CLK;  
end 
initial begin 
S = 1'b1;
R = 1'b0;
#100
S = 1'b0;
R = 1'b1;
#100
S = 1'b0;
R = 1'b0;
#100
S = 1'b1;
R = 1'b1;
#100
$finish;

end 
endmodule