module ex_3(A,B,fa);
    input A,B;
    output fa;
    assign fa = ((A&B) | ~(A&B));
endmodule