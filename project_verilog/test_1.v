module test_1(A,B,fa);
    input A,B;
    output fa;
    assign fa = ~(~(A&A) & ~(B&B));
endmodule