module JK_flipflop(q,qbar,clk,j,k);

input j,k,clk;
output q,qbar;



wire w1,w2,w3,w4,w5,w6,w7,w8;

nand(w1,j,qbar,clk);
nand(w2,k,q,clk);
nand(w3,w1,w6);
nand(w4,w2,w5);
nand(w7,w5,~clk);
nand(w8,w6,~clk);
nand(q,w7,qbar);
nand(qbar,w8,q);



endmodule
